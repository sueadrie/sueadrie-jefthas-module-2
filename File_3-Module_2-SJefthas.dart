void main () {
  
  var appWinnerOf2021 = AppOfTheYearWinner(
    "Ambani Africa",
    "Best South African solution, Best gaming solution, Best educational solution",
    "Mukundi Lambani",
    2021
  );
  
  var appWinnerOf2020 = AppOfTheYearWinner(
    "EasyEquities",
    "Best Consumer Solution",
    "First World Trader",
    2020
  );
  
  var appWinnerOf2019 = AppOfTheYearWinner(
    "Naked Insurance",
    "Best Financial Solution Award",
    "Sumarie Greybe and Ernest North",
    2019
  );
  
  var appWinnerOf2018 = AppOfTheYearWinner(
    "Khula",
    "Best Agriculture Solution",
    "Karidas Tshintsholo, Matthew Piper and Jackson Dyora",
    2018
  );
  
  var appWinnerOf2017 = AppOfTheYearWinner(
    "Shyft",
    "Best Financial Solution",
    "Standard Bank",
    2017
  );
  
  var appWinnerOf2016 = AppOfTheYearWinner(
    "Domestly",
    "Best Consumer App",
    "Berno Potgieter and Thatoyaona Marumo",
    2016
  );
  
  var appWinnerOf2015 = AppOfTheYearWinner(
    "WumDrop",
    "Best Enterprise",
    "WumWum",
    2015
  );
  
  var appWinnerOf2014 = AppOfTheYearWinner(
    "LIVE Inspect",
    "Best Android Enterprise App",
    "Lightstone",
    2014
  );
  
  var appWinnerOf2013 = AppOfTheYearWinner(
    "SnapScan",
    "Best HTML 5 app",
    "Kobus Ehlers",
    2013
  );
  
  var appWinnerOf2012 = AppOfTheYearWinner(
    "FNB Banking App",
    "Best iOS consumer App, Best Blackberry App, Best Android consumer App",
    "FNB",
    2012
  );

  appWinnerOf2021.displayAppWinnerDetails();
  appWinnerOf2020.displayAppWinnerDetails();
  appWinnerOf2019.displayAppWinnerDetails();
  appWinnerOf2018.displayAppWinnerDetails();
  appWinnerOf2017.displayAppWinnerDetails();
  appWinnerOf2016.displayAppWinnerDetails();
  appWinnerOf2015.displayAppWinnerDetails();
  appWinnerOf2014.displayAppWinnerDetails();
  appWinnerOf2013.displayAppWinnerDetails();
  appWinnerOf2012.displayAppWinnerDetails();
}

 class AppOfTheYearWinner {
   
   String appName = "";
   String appCategory = "";
   String appDeveloper = "";
   int appWinningYear = 0;  
   
   void appNameToUppercase (appName) {
     String appNameConvertedToUppercase = appName.toUpperCase();
     print("App Name: $appNameConvertedToUppercase");
   }
      
   AppOfTheYearWinner (String appName, String appCategory, String appDeveloper, int appWinningYear) {
     this.appName = appName;
     this.appCategory = appCategory;
     this.appDeveloper = appDeveloper;
     this.appWinningYear = appWinningYear;
   }
    
   void displayAppWinnerDetails () {
     appNameToUppercase(appName);
     print("Winning Category: $appCategory");
     print("App Developer: $appDeveloper");
     print("Winning Year: $appWinningYear");  
     print("\n");
   }
} 