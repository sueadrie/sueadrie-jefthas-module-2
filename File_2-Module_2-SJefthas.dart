//Save list of MTN App of the Year winners since 2012
//Display list of winners sorted according to name
//Display winning apps of 2017 and 2018
//Display total number of winning apps

void main () {
  
    List<String> winnersList = [
      "FNB Banking App: 2012",
      "SnapScan: 2013",
      "LIVE Inspect: 2014",
      "WumDrop: 2015",
      "Domestly: 2016",
      "Shyft: 2017",
      "Khula: 2018",
      "Naked Insurance: 2019",
      "EasyEquities: 2020",
      "Ambani Africa: 2021"
    ];

    String winnerOf2017 = winnersList[5];
    String winnerOf2018 = winnersList[6];
    int totalNumberOfApps = winnersList.length;
  
    winnersList.sort();

    print("This is the list of MTN App of the Year overall winners since 2012:");

    for (String winner in winnersList) {
        print(winner);
    }
    print("\n");

    print("Overall Winning App: $winnerOf2017");
    print("Overall Winning App: $winnerOf2018");
  
    print("\n");
  
    print("The total number of overall winners since 2012 is $totalNumberOfApps."); 
}